package br.com.novapat.api.cliente.controller;


import br.com.novapat.api.cliente.models.Cliente;
import br.com.novapat.api.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping("/cliente")
    public Cliente criarCliente (@RequestBody Cliente cliente){
        return clienteService.create(cliente);
    }

    @GetMapping("cliente/{id}")
    public Optional<Cliente> buscarPorId(@PathVariable Long id){
        return clienteService.findById(id);
    }

}
