package br.com.novapat.api.cliente.repository;

import br.com.novapat.api.cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {
}
